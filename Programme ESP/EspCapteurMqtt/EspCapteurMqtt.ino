#include <Adafruit_SSD1306.h>
#include <splash.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <PubSubClient.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// ADXL345 I2C address is 0x53(83)
#define Addr 0x53



// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);


const char* ssid = "FREEBOX_RUDY_TL";
const char* password =  "F4CAE55E55C1";
const char* mqtt_server = "192.168.0.36";
WiFiClient espClient;
PubSubClient client(espClient);

double Xini;
  double Yini; 
  double Zini;


void ini(){
  
  unsigned int data[6];

  // Start I2C Transmission
  Wire.beginTransmission(Addr);
  // Select bandwidth rate register
  Wire.write(0x2C);
  // Normal mode, Output data rate = 100 Hz ------------------------------------------------------------------------------------------ defaut 0x0A  B car 200 hz
  Wire.write(0x0B);
  // Stop I2C transmission
  Wire.endTransmission();

  // Start I2C Transmission
  Wire.beginTransmission(Addr);
  // Select power control register
  Wire.write(0x2D);
  // Auto-sleep disable
  Wire.write(0x08);
  // Stop I2C transmission
  Wire.endTransmission();

  // Start I2C Transmission
  Wire.beginTransmission(Addr);
  // Select data format register
  Wire.write(0x31);
  // Self test disabled, 4-wire interface, Full resolution, Range = +/-2g -------------------------------------------------------------- defaut 0x08 B car 16g
  Wire.write(0x0B);
  // Stop I2C transmission
  Wire.endTransmission();
//  delay(300);

  for (int i = 0; i < 6; i++)
  {
    // Start I2C Transmission
    Wire.beginTransmission(Addr);
    // Select data register
    Wire.write((50 + i));
    // Stop I2C transmission
    Wire.endTransmission();

    // Request 1 byte of data
    Wire.requestFrom(Addr, 1);

    // Read 6 bytes of data
    // xAccl lsb, xAccl msb, yAccl lsb, yAccl msb, zAccl lsb, zAccl msb
    if (Wire.available() == 1)
    {
      data[i] = Wire.read();
    }
  }

  // Convert the data to 10-bits
  double xAccl = (((data[1] & 0x03) * 256) + data[0]);
  if (xAccl > 511)
  {
    xAccl -= 1024;
  }
  double  yAccl = (((data[3] & 0x03) * 256) + data[2]);
  if (yAccl > 511)
  {
    yAccl -= 1024;
  }
  double  zAccl = (((data[5] & 0x03) * 256) + data[4]);
  if (zAccl > 511)
  {
    zAccl -= 1024;
  }
Xini = xAccl /256 ;
Yini = yAccl /256;
Zini = zAccl /256;

}

void setup() {
  Serial.begin(115200);
  Wire.begin(5, 4);
  WiFi.begin(ssid, password);

  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for (;;);
  }
  delay(2000);
  display.clearDisplay();
  wifi();
  delay(5000);
  mqttConnection();
  client.subscribe("test");
  
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0, 10);
  ini();
  for(;;){
      capteur();
    }
}

void loop() {
}

void wifi() {
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 10);
  display.println("Connecting to WiFi..");
  display.display();
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }

  Serial.println("Connected to the WiFi network");
  display.clearDisplay();
  display.setCursor(0, 10);
  display.println("Connected to the WiFi network");
  display.display();
}

void mqttConnection() {
  display.clearDisplay();
  Serial.println("Connecting to MQTT Server..");
  display.setCursor(0, 10);
  display.println("Connecting to MQTT Server..");
  display.display();
  delay(2000);
  client.setServer(mqtt_server, 1883);
  if (client.connect("EspClientTheo")) {
    Serial.println("Connected to the MQTT Server");
    display.clearDisplay();
    display.setCursor(0, 10);
    display.println("Connected to the MQTT Server");
    display.display();

  } else {
    Serial.print("failed, rc=");
    Serial.print(client.state());
    display.clearDisplay();
    display.setCursor(0, 10);
    display.print("failed, rc=");
    display.print(client.state());
    display.display();
    delay(10000000);
  }
}


void capteur()
{
   unsigned int data[6];

  // Start I2C Transmission
  Wire.beginTransmission(Addr);
  // Select bandwidth rate register
  Wire.write(0x2C);
  // Normal mode, Output data rate = 100 Hz ------------------------------------------------------------------------------------------ defaut 0x0A  B car 200 hz
  Wire.write(0x0B);
  // Stop I2C transmission
  Wire.endTransmission();

  // Start I2C Transmission
  Wire.beginTransmission(Addr);
  // Select power control register
  Wire.write(0x2D);
  // Auto-sleep disable
  Wire.write(0x08);
  // Stop I2C transmission
  Wire.endTransmission();

  // Start I2C Transmission
  Wire.beginTransmission(Addr);
  // Select data format register
  Wire.write(0x31);
  // Self test disabled, 4-wire interface, Full resolution, Range = +/-2g -------------------------------------------------------------- defaut 0x08 B car 16g
  Wire.write(0x0B);
  // Stop I2C transmission
  Wire.endTransmission();
//  delay(300);

  for (int i = 0; i < 6; i++)
  {
    // Start I2C Transmission
    Wire.beginTransmission(Addr);
    // Select data register
    Wire.write((50 + i));
    // Stop I2C transmission
    Wire.endTransmission();

    // Request 1 byte of data
    Wire.requestFrom(Addr, 1);

    // Read 6 bytes of data
    // xAccl lsb, xAccl msb, yAccl lsb, yAccl msb, zAccl lsb, zAccl msb
    if (Wire.available() == 1)
    {
      data[i] = Wire.read();
    }
  }

  // Convert the data to 10-bits
  double xAccl = (((data[1] & 0x03) * 256) + data[0]);
  if (xAccl > 511)
  {
    xAccl -= 1024;
  }
  double  yAccl = (((data[3] & 0x03) * 256) + data[2]);
  if (yAccl > 511)
  {
    yAccl -= 1024;
  }
  double  zAccl = (((data[5] & 0x03) * 256) + data[4]);
  if (zAccl > 511)
  {
    zAccl -= 1024;
  }

// diviser par 256 pour avoir en G
// -1 sur l'axe Z car pesanteur

//  Output data to serial monitor
  Serial.print("X-Axis : ");
  Serial.println((xAccl/256)-Xini);
  Serial.print("Y-Axis : ");
  Serial.println((yAccl/256)-Yini);
  Serial.print("Z-Axis : ");
  Serial.println((zAccl/256)-Zini);

display.clearDisplay();
display.setCursor(0, 10);
  display.print("X : ");
  display.println((xAccl/256)-Xini);
  display.print("Y : ");
  display.println((yAccl/256)-Yini);
  display.print("Z : ");
  display.println((zAccl/256)-Zini);
  display.display(); 
  delay(30);

//creation json
StaticJsonBuffer<300> JSONbuffer;
JsonObject& JSONencoder = JSONbuffer.createObject();

JSONencoder["NomCapt"] = "Test1";
 JsonArray& values = JSONencoder.createNestedArray("valeurs");
 
 values.add((xAccl/256)-Xini);
 values.add((yAccl/256)-Yini);
 values.add((zAccl/256)-Zini);

char JSONmessageBuffer[300];
  JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
//test d'envoie en MQTT
 



client.publish("test", JSONmessageBuffer );
  //delay(30);
}
